<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Activity S05</title>
	</head>
	<body>

		<?php session_start(); ?>


		<?php if(isset($_SESSION['users'])): ?>


			<p>Hello, <?php echo $_SESSION['users'][0]->email ?></p>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="logout">
				<button type="submit">Logout</button>
			</form>


		<?php else: ?>

			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="login">
				Email: <input type="email" name="email" required>
				Password: <input type="password" name="password" required>
				<button type="submit">Login</button>
			</form>

		<?php endif; ?>
		
	</body>
</html>
